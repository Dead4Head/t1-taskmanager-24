package ru.t1.amsmirnov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password
    ) throws AbstractException;

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    ) throws AbstractException;

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role
    ) throws AbstractException;

    @NotNull
    User removeByLogin(@Nullable String login) throws AbstractException;

    @NotNull
    User removeByEmail(@Nullable String email) throws AbstractException;

    @NotNull
    User setPassword(
            @Nullable String id,
            @Nullable String password
    ) throws AbstractException;

    @NotNull
    User updateUserById(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    ) throws AbstractException;

    void lockUserByLogin(@Nullable String login) throws AbstractException;

    void unlockUserByLogin(@Nullable String login) throws AbstractException;

    @NotNull
    User findOneByLogin(@Nullable String login) throws AbstractException;

    @NotNull
    User findOneByEmail(@Nullable String email) throws AbstractException;

    boolean isLoginExist(@Nullable String login) throws AbstractException;

    boolean isEmailExist(@Nullable String email) throws AbstractException;

}
