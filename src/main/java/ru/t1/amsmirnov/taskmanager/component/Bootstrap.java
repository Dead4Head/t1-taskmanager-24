package ru.t1.amsmirnov.taskmanager.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.t1.amsmirnov.taskmanager.api.repository.ICommandRepository;
import ru.t1.amsmirnov.taskmanager.api.repository.IProjectRepository;
import ru.t1.amsmirnov.taskmanager.api.repository.ITaskRepository;
import ru.t1.amsmirnov.taskmanager.api.repository.IUserRepository;
import ru.t1.amsmirnov.taskmanager.api.service.*;
import ru.t1.amsmirnov.taskmanager.command.AbstractCommand;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.system.ArgumentNotSupportedException;
import ru.t1.amsmirnov.taskmanager.exception.system.CommandNotSupportedException;
import ru.t1.amsmirnov.taskmanager.model.User;
import ru.t1.amsmirnov.taskmanager.repository.CommandRepository;
import ru.t1.amsmirnov.taskmanager.repository.ProjectRepository;
import ru.t1.amsmirnov.taskmanager.repository.TaskRepository;
import ru.t1.amsmirnov.taskmanager.repository.UserRepository;
import ru.t1.amsmirnov.taskmanager.service.*;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

import java.lang.reflect.Modifier;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.amsmirnov.taskmanager.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    @Getter
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    @Getter
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    @Getter
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    @Getter
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    @Getter
    private final IUserService userService = new UserService(userRepository, projectService, taskService);

    @NotNull
    @Getter
    private final IAuthService authService = new AuthService(userService);

    @NotNull
    @Getter
    private final ILoggerService loggerService = new LoggerService();

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);
    }

    private void initDemoData() {
        try {
            @NotNull User admin = userService.create("admin", "admin", Role.ADMIN);
            @NotNull User test = userService.create("test", "test", "test@test.test");
            @NotNull User user = userService.create("user", "user");

            projectService.create(test.getId(), "Project 1", "Description 1");
            projectService.create(test.getId(), "Project 2", "Description 2");
            projectService.create(test.getId(), "Project 3", "Description 3");
            projectService.create(admin.getId(), "Admin project", "Description of the admin project");

            taskService.create(test.getId(), "TASK 1", "TASK 1");
            taskService.create(admin.getId(), "TASK 2", "TASK 2");
            taskService.create(user.getId(), "TASK 3", "TASK 3");

        } catch (@NotNull final Exception exception) {
            renderError(exception);
        }
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(() ->
                loggerService.info("** TASK MANAGER IS SHUTTING DOWN **"))
        );
    }

    private void renderError(@NotNull Exception exception) {
        loggerService.error(exception);
        System.out.println("[FAIL]");
    }

    public void start(@Nullable String[] args) {
        if (processArguments(args)) exit();

        initDemoData();
        initLogger();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND: ");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception exception) {
                renderError(exception);
            }
        }
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        getCommandService().add(command);
    }

    public boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String argument = args[0];
        try {
            processArgument(argument);
        } catch (@NotNull final Exception exception) {
            renderError(exception);
        }
        return true;
    }

    public void processArgument(@Nullable final String argument) throws AbstractException {
        @Nullable final AbstractCommand command = commandService.getCommandByArgument(argument);
        if (command == null) throw new ArgumentNotSupportedException(argument);
        command.execute();
    }

    public void processCommand(@NotNull final String commandName) throws AbstractException {
        @Nullable final AbstractCommand command = commandService.getCommandByName(commandName);
        if (command == null) throw new CommandNotSupportedException();
        authService.checkRoles(command.getRoles());
        command.execute();
    }

    public void exit() {
        System.exit(0);
    }

}
